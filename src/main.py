import pygame
import sys
import time
import random

from pygame.locals import *

import Game
import Scale
import City

FPS = 60
SCREEN_X = 1200
SCREEN_Y = 800

screen = None
surface = None
mapImg = None
mapImgScaled = None

scale = None
needsRescale = True

game = None

selectedObject = None

def start():
	global screen, surface, mapImg, mapImgScaled, scale, game

	pygame.init()
	screen = pygame.display.set_mode((SCREEN_X, SCREEN_Y), 0, 32)
	surface = pygame.Surface(screen.get_size())
	surface = surface.convert()
	surface.fill((0, 0, 0))

	pygame.key.set_repeat(1, 200)

	mapImg = pygame.image.load("assets/map.png")
	scale = Scale.Scale(mapImg.get_size()[0], mapImg.get_size()[1], SCREEN_X, SCREEN_Y)
	mapImgScaled = pygame.transform.smoothscale(mapImg, (int(mapImg.get_size()[0] / scale.getScales()[0]), int(mapImg.get_size()[1] / scale.getScales()[1])))

	screen.blit(surface, (0, 0))

	game = Game.Game()

def loop():
	global needsRescale, mapImgScaled, selectedObject

	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			game.save()
			pygame.quit()
			quit()
		if event.type == pygame.KEYDOWN:
			if event.key == pygame.K_KP_PLUS:
				scale.zoomIn()
				needsRescale = True
			if event.key == pygame.K_KP_MINUS:
				scale.zoomOut()
				needsRescale = True
			if event.key == pygame.K_KP8: scale.move(0, 1)     # Up
			if event.key == pygame.K_KP2: scale.move(0, -1)    # Down
			if event.key == pygame.K_KP4: scale.move(1, 0)     # Left
			if event.key == pygame.K_KP6: scale.move(-1, 0)    # Right
		if event.type == pygame.MOUSEBUTTONUP:
			print("Mouse Event!")
			game.deselectAll()
			selectedObject = None
			print(scale.toMap(event.pos))
			#game.cities.append(City.City(scale.toMap(event.pos)[0], scale.toMap(event.pos)[1]))
			for city in game.cities:
				if city.isClicked(scale.toMap(event.pos)) == True:
					city.selected(True)
					selectedObject = city

	if needsRescale == True:
		print("Rescaling map...")
		mapImgScaled = pygame.transform.smoothscale(mapImg, (int(mapImg.get_size()[0] / scale.getScales()[0]), int(mapImg.get_size()[1] / scale.getScales()[1])))
		needsRescale = False
	surface.blit(mapImgScaled, scale.getOffsets())

	for road in game.roads:
		road.display(surface, scale)
	for city in game.cities:
		city.display(surface, scale)
	if selectedObject != None:
		selectedObject.display(surface, scale)

start()
print("Game init complete!")
while(True):
	surface.fill((0, 0, 0))
	screen.blit(surface, (0, 0))
	loop()
	screen.blit(surface, (0, 0))
	pygame.display.flip()
	pygame.display.update()
