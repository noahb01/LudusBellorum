import pickle
from pathlib import Path

import City
import Player
import Resource
import Road

class Game:
	cities = None
	players = None
	roads = None

	def __init__(self):
		try:
			with open("saves/meta.dat") as meta:
				if meta.readline() == "LudusBellorum Savefile v1.0\n":
					print("Found vaild savefile, loading...")
					with open("saves/cities.dat", "rb") as city_save:
						self.cities = pickle.load(city_save)
					with open("saves/players.dat", "rb") as player_save:
						self.players = pickle.load(player_save)
					with open("saves/roads.dat", "rb") as road_save:
						self.roads = pickle.load(road_save)
				else:
					print("Savefile present, but metadata is corrupted!")
		except IOError as e:
			print("No savefile found, starting new game...")

			print("Loading defaut city map...")
			with open("assets/cities.dat", "rb") as city_save:
				self.cities = pickle.load(city_save)

			self.players = [Player.Player("Rome", (180, 10, 2))]
			self.players.append(Player.Player("Africa", (139, 69, 19)))
			self.players.append(Player.Player("Greece", (13, 94, 175)))
			self.players.append(Player.Player("Egypt", (192, 147, 0)))

			self.cities.append(City.City(2193, 1940, name = "Roma", type = City.CITY_LARGE, owner = self.players[0]))
			self.cities.append(City.City(2099, 2513, name = "Africa", type = City.CITY_LARGE, owner = self.players[1]))
			self.cities.append(City.City(3354, 1889, name = "Gracea", type = City.CITY_LARGE, owner = self.players[2]))
			self.cities.append(City.City(4782, 3489, name = "Aegyptus", type = City.CITY_LARGE, owner = self.players[3]))

			self.roads = [Road.Road(self.cities[0], self.cities[0])]

	def save(self):
		print("Saving game...")
		with open("saves/meta.dat", "w") as meta:
			print("LudusBellorum Savefile v1.0\n", file = meta)
		with open("saves/cities.dat", "wb") as city_save:
			pickle.dump(self.cities, city_save)
		with open("saves/players.dat", "wb") as player_save:
			pickle.dump(self.players, player_save)
		with open("saves/roads.dat", "wb") as road_save:
			pickle.dump(self.roads, road_save)

	def deselectAll(self):
		for city in self.cities:
			city.selected(False)
