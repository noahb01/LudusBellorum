import pygame

import City
import Scale

ROAD_ROAD = 0

class Road:
	endpoints = None
	type = None

	def __init__(self, city1, city2, type = ROAD_ROAD):
		self.endpoints = [city1, city2]
		self.type = type

	def display(self, surface, scale):
			e1 = scale.toDisplay(self.endpoints[0].loc_x, self.endpoints[0].loc_y)
			e2 = scale.toDisplay(self.endpoints[1].loc_x, self.endpoints[1].loc_y)

			pygame.draw.line(surface, (0, 0, 0), e1, e2, 5)
			pygame.draw.line(surface, (255, 255, 255), e1, e2, 2)
