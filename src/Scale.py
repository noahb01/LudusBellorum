# MAP SCALING
#
# Scale of 1 means a 1 to 1 pixel mapping
#
# Equation is as follows
#           (map_dim)
# scale = -------------
#          (screen_dim)
#
# Higher values = more zoomed out
# Lower values = zoomed in

class Scale:
	scaleX = 1.0
	scaleY = 1.0
	offsetX = 0
	offsetY = 0

	def __init__(self, mapX, mapY, screenX, screenY):
		self.scaleX = mapX / screenX
		self.scaleY = mapY / screenY
		print("Using map scaling of ({}, {}).".format(self.scaleX, self.scaleY))

	def getScales(self):
		return (self.scaleX, self.scaleY)

	def getOffsets(self):
		return (self.offsetX, self.offsetY)

	def zoomIn(self):
		self.scaleX = self.scaleX * 0.9
		self.scaleY = self.scaleY * 0.9

	def zoomOut(self):
		self.scaleX = self.scaleX * 1.1
		self.scaleY = self.scaleY * 1.1

	def toDisplay(self, mapX, mapY):
		realX = mapX * (1 / self.scaleX) + self.offsetX
		realY = mapY * (1 / self.scaleY) + self.offsetY
		return (int(realX), int(realY))

	def toMap(self, disp):
		(dispX, dispY) = disp
		return (dispX * self.scaleX - (self.offsetX * self.scaleX), dispY * self.scaleY - (self.offsetY * self.scaleY))

	def move(self, movX, movY):
		self.offsetX += movX * 20
		self.offsetY += movY * 20
