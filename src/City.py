import pygame

import Scale

CITY_SMALL = 0
CITY_LARGE = 1
CITY_ORACLE = 2
CITY_FAKE = 3

class City:
	name = ""
	loc_x = 0
	loc_y = 0
	owner = None
	type = CITY_SMALL
	color = (255, 255, 255)
	size = 8
	rss = None
	isselected = False

	def __init__(self, loc_x, loc_y, name = "", type = CITY_SMALL, owner = None):
		self.loc_x = loc_x
		self.loc_y = loc_y
		self.owner = owner
		self.name = name
		self.type = type
		if self.type == CITY_LARGE:
			self.size = 15

	def display(self, surface, scale):
		(realX, realY) = scale.toDisplay(self.loc_x, self.loc_y)

		if self.isselected == True:
			pygame.draw.circle(surface, (255, 0, 0), (realX, realY), self.size + 2)

		pygame.draw.circle(surface, (0, 0, 0), (realX, realY), self.size)
		if self.owner != None:
			self.color = self.owner.color
		pygame.draw.circle(surface, self.color, (realX, realY), self.size - 2)

		if self.type == CITY_LARGE:
			pygame.draw.circle(surface, (255, 255, 255), (realX, realY), 3)

	def isClicked(self, pos):
		(posX, posY) = pos

		distX = int(abs(posX - self.loc_x))
		distY = int(abs(posY - self.loc_y))

		if (distX ^ 2) + (distY ^ 2) <= (self.size ^ 2):
			return True
		return False

	def isSelected(self):
		return self.isselected

	def selected(self, val):
		if val == True:
			print("City selected")
		self.isselected = val
